angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [
  {
    id: 0,
    name: 'Family',
    lastText: 'John, Remember the soup I used to cook for you as a kid, I made some today.',
    face: 'http://www.dm.org/events/summer-internship/Geiger_Family.png/image_preview',
    chatdetails: [
    {
      name: 'Mother',
      text: 'John, Remember the soup I used to cook for you as a kid, I made some today.',
      face: 'http://www.ideachampions.com/weblogs/mean-old-lady.jpg',
      picture: 'img/soup.jpg',
      date: 'March 01, 2015',
      loves: '2 loves',
      kisses: '3 Kisses',
      poops: '0 Poops',
      comments: '1 Comments'
    },
    {
      name: 'Father',
      text: 'Haha, John. This is us having the soup together',
      face: 'https://d262ilb51hltx0.cloudfront.net/max/800/1*VvEt7cN1_WW1SOFzsvSsiQ.jpeg',
      picture: 'img/soupus.jpg',
      date: 'March 01, 2015',
      loves: '1 loves',
      kisses: '5 Kisses',
      poops: '7 Poops',
      comments: '1 Comments'
    },
    {
      name: 'Me',
      text: 'This is me having the same soup in a grand restaurant.',
      face: 'http://cdn2.freepik.com/image/th/318-52014.png',
      picture: 'img/soupyou.jpg',
      date: 'March 02, 2015',
      loves: '1 love',
      kisses: '1 Kiss',
      poops: '0 Poops',
      comments: '0 Comments'
    },
    {
      name: 'Father',
      text: 'Give me some of that soup',
      face: 'https://d262ilb51hltx0.cloudfront.net/max/800/1*VvEt7cN1_WW1SOFzsvSsiQ.jpeg',
      picture: 'img/dog.jpg',
      date: 'March 02, 2015',
      loves: '0 loves',
      kisses: '0 Kisses',
      poops: '1 Poop',
      comments: '0 Comments'
    }]
  }, {
    id: 1,
    name: 'Britney Spears',
    lastText: 'If you don\'t come here, I\'m going to delete all my old pictures',
    face: 'http://37.media.tumblr.com/avatar_2d2047f18569_128.png',
    chatdetails: [
    {
      name: 'Britney Spears',
      text: 'Hey, want to go out for Karaoke tonight?',
      face: 'http://37.media.tumblr.com/avatar_2d2047f18569_128.png',
      picture: 'img/karaoke.jpg',
      date: 'March 03, 2015',
      loves: '1 Love',
      kisses: '1 Kiss',
      poops: '1 Poop',
      comments: '5 Comments'
    },
    {
      name: 'Britney Spears',
      text: 'If you don\'t come here, I\'m going to delete all my old pictures',
      face: 'http://37.media.tumblr.com/avatar_2d2047f18569_128.png',
      picture: 'img/comehere.jpg',
      date: 'March 03, 2015',
      loves: '0 Love',
      kisses: '0 Kiss',
      poops: '10 Poop',
      comments: '0 Comments',
    }]
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Blocked out message',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg',
    chatdetails: [
    {
      name: 'Andrew Jostlin',
      text: 'Blocked out message',
      face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg',
      picture: 'img/lock.jpg',
      date: 'March 01, 2015',
      loves: '4 loves',
      kisses: '10 Kisses',
      poops: '5 Poops',
      comments: '2 Comments'
    }]
  }, {
      id: 3,
      name: 'Adam Bradleyson',
      lastText: 'Did you know Katy\'s really sick? Come meet her.',
      face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
      chatdetails: [
      {
        name: 'Adam Bradleyson',
        text: 'Did you know Katy\'s really sick? Come meet her.',
        face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
        picture: 'img/sick.jpg',
        date: 'February 13, 2015',
        loves: '0 loves',
        kisses: '3 Kises',
        poops: '0 Poops',
        comments: '0 Comments'
      }]
    }, {
      id: 4,
      name: 'Perry Governor',
      lastText: 'One ring to rule them all.',
      face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg',
      chatdetails: [
      {
        name: 'Perry Governor',
        text: 'One ring to rule them all.',
        face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg',
        picture: 'img/ring.jpg',
        date: 'March 02,2015',
        loves: '3 loves',
        kisses: '0 Kisses',
        poops: '15 Poops',
        comments: '10 Comments'
      }]
    }];

    return {
      all: function() {
        return chats;
      },
      remove: function(chat) {
        chats.splice(chats.indexOf(chat), 1);
      },
      get: function(chatId) {
        for (var i = 0; i < chats.length; i++) {
          if (chats[i].id === parseInt(chatId)) {
            return chats[i];
          }
        }
        return null;
      }
    };
  });

